<?php
session_start();
?>
<!DOCTYPE html>
<html>
<head>
	 <meta charset="UTF-8">
	  <meta name="description" content="movies">
	  <meta name="keywords" content="movies,genre,store">
	  <meta name="author" content="Joseph Mbugua">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rhoxi Movie Store</title>
	<!-- <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.min.css"> -->
	<link rel="stylesheet" type="text/css" href="css/style.css">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body style="background-image: url('images/body.jpg');">

    <br>
	<div class="container">
       <div id="one">
		<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><img src="images/icon.png" style="width: 40px; height: 40px;"></a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#">View Movies</a>
      </li>
  
    </ul>
 
  </div>
</nav>
       </div>

       <div class="jumbotron" style="margin-top: 10px;">
       	    <div class="container">
       	    	  <div class="row">
       	    	  	  <div class="col-sm" id="two">
       	    	  	  	  <h4>Sign In to Account</h4>
                        <h4>
                          <?php
                          if (isset($_GET['notverified'])) {
                            # code...
                            if (isset($_SESSIONS['notverified'])) {
                              # code...
                              echo $_SESSIONS['notverified'];
                                   session_unset();
                              session_destroy();
                            } else {
                              echo "Account not yet verified";
                            }
                          }

                           if (isset($_GET['notrec'])) {
                            # code...
                            if (isset($_SESSIONS['notrec'])) {
                              # code...
                              echo $_SESSIONS['notrec'];
                                   session_unset();
                              session_destroy();
                            } else {
                              echo "Account doesnt exist";
                            }
                          }

                          if (isset($_GET['resetpass'])) {
                            # code...
                            echo "password reset is Successfull";
                          }

                          ?>
                        </h4>
       	    	  	  	  <form action="public/authentication/login.php" method="post">
       	    	  	  	  	    <div class="form-group">
       	    	  	  	  	    	<input type="email" name="email" placeholder="Enter email" class="form-control" required="">
       	    	  	  	  	    </div>
       	    	  	  	  	       <div class="form-group">
       	    	  	  	  	    	<input type="password" name="password" placeholder="Enter password" class="form-control" required="">
       	    	  	  	  	    </div>
       	    	  	  	  	       <div class="form-group">
       	    	  	  	  	    	<input type="submit" name="login" class="btn btn-primary btn-block" value="Login" >
       	    	  	  	  	    </div>
       	    	  	  	  	    <div class="form-group">
       	    	  	  	  	    	<p>Forgot password ? <a href="public/views/forgotpass.php">Click</a> here to reset</p>
       	    	  	  	  	    </div>
       	    	  	  	  </form>
       	    	  	  </div>
       	    	  	  <div class="col-sm" id="three">
       	    	  	  	<h4>Create Account</h4>
                      <h4>
                        <?php
                          if (isset($_GET['userReg'])) {
                            # code...
                            if (isset($_SESSIONS['regsuccess'])) {
                              # code...
                              echo $_SESSIONS['regsuccess'];
                                   session_unset();
                              session_destroy();
                            } else {
                              echo "Registration SuccessFull";
                            }
                          }

                           if (isset($_GET['notReg'])) {
                            # code...
                            if (isset($_SESSIONS['regnot'])) {
                              # code...
                              echo $_SESSIONS['regnot'];
                                   session_unset();
                              session_destroy();
                            } else {
                              echo "Registration not SuccessFull";
                            }
                          }


                           if (isset($_GET['usertaken'])) {
                            # code...
                            if (isset($_SESSIONS['userTaken'])) {
                              # code...
                              echo $_SESSIONS['userTaken'];
                              session_unset();
                              session_destroy();
                            } else {
                              echo "Credentials already exists, create account with new info";
                            }
                          }

                        ?>

                      </h4>
                       <form action="public/authentication/registration.php" method="post" enctype="multipart/form-data">
       	    	  	  	  	    <div class="form-group">
       	    	  	  	  	    	<input type="email" name="email" placeholder="Enter email" class="form-control" required="">
       	    	  	  	  	    </div>
       	    	  	  	  	        <div class="form-group">
       	    	  	  	  	    	<input type="text" name="moviestore" placeholder="Enter Movie Store name" class="form-control" required="">
       	    	  	  	  	    </div>
                              <div class="form-group">
                              <label for="selectUser">Select User Type</label>
                              <select class="form-control" id="selectUser" name="usertype">
                                  <option value="user">Normal User</option>
                                  <option value="admin">Movie Store Owner</option>
                              </select>
                              </div>
       	    	  	  	  	       <div class="form-group">
       	    	  	  	  	    	<input type="password" onkeyup="check()" id="passwordcreate" name="password" placeholder="Enter password" class="form-control" required="">
       	    	  	  	  	    </div>
       	    	  	  	  	      <div class="form-group">
       	    	  	  	  	    	<input type="password" name="conpassword" id="conpasscreate" onkeyup="check()" placeholder="Confirm password" class="form-control" required="">
                                <span id="message"></span>

       	    	  	  	  	    </div>
       	    	  	  	  	       <div class="form-group">
       	    	  	  	  	    	<input type="submit" name="register" id="createAcc" class="btn btn-primary btn-block" value="Create Account" >
       	    	  	  	  	    </div>
       	    	  	  	  	      <div class="form-group">
       	    	  	  	  	    	<input type="reset" name="reset" class="btn btn-danger btn-block" value="Reset Form" >
       	    	  	  	  	    </div>
       	    	  	  	  </form>       	    
       	    	  	  	  	  	  </div>
       	    	  </div>
       	    </div>
       </div>

       <div  id="four">
       	   copyright Joseph @ <?php echo date('Y'); ?>
       </div>
		
	</div>


<script type="text/javascript">
    //js inside html 
    //function check 
    function check(){
      if (document.getElementById('passwordcreate').value === document.getElementById('conpasscreate').value) {

        //interface changes
        document.getElementById('message').style.color = "green";
        document.getElementById('message').innerHTML = "Passwords match";
        document.getElementById('createAcc').disabled = false;

      } else {
        document.getElementById('message').style.color = "red";
        document.getElementById('message').innerHTML = "Passwords do not match";
        document.getElementById('createAcc').disabled = true;
      }
    }
</script>
<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>