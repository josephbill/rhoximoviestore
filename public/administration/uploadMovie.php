<?php 
//start session
session_start();
//including our mysql database connection
include '../../connection.php';

// if ($conn->connect_error) {
// 	# code...
// 	echo "connection not established";
// } else {
// 	echo "connection is established";
// }

//declare variables to store our inputs
$movie_name = '';
$movie_duration = '';
$movie_genre = '';
$movie_image = '';
$storage_image = '';
$movie_price = '';
$updateid = '';
$target = ''; //basically for our image storage 


//declare error variables 
$errorMessage = '';

//isset to check to whether the submit button of form has been clicked 
if (isset($_POST['save'])) {
	# code...
	//capture the user input and validate the data 
	if (empty($_POST['moviename']) || empty($_POST['movielength']) || empty($_POST['moviegenre']) || empty($_POST['movieprice']) ) {
		# code...
		$errorMessage = "fields cannot be empty";
	} else {
		$movie_name = $_POST['moviename'];
	    $movie_duration = $_POST['movielength'];
	    $movie_genre = $_POST['moviegenre'];
	    $movie_price = $_POST['movieprice'];
	    $movie_image = $_FILES['movieposter']['name'];
	}

	//target notation
	$target = "../../movieimages/" .basename($_FILES['movieposter']['name']);
	$storage_image = $_FILES['movieposter']['tmp_name'];
	//move the uploaded file to storage folder 
	if (move_uploaded_file($storage_image, $target) === TRUE) {
		# code...
		echo "<script>alert('image moved');</script>";
	} else {
				echo "<script>alert('image not moved');</script>";

	}
	// move_uploaded_file($storage_image, $target);

	if (empty($errorMessage)) {
			# code...
			//prepare our statement and take data to database 
	   $stmt = $conn->prepare("INSERT INTO movies (moviename,movieduration,moviegenre,movieImage,movieprice) VALUES (?,?,?,?,?)");

	//bind the parameters 
	$stmt->bind_param('sssss',$movie_name,$movie_duration,$movie_genre,$movie_image,$movie_price);

	//execute the statement 
	if ($stmt->execute() === TRUE) {
		# code...
		// echo $conn->insert_id;
		// echo "data is inserted<br>";
		$_SESSIONS['success'] = "movie uploaded";
		$_SESSIONS['classtype'] = "success";
		//redirect 
		header('location: ../views/uploadMovieView.php?success');
	} else {
		$_SESSIONS['failure'] = "movie not uploaded";
	    header('location: ../views/uploadMovieView.php?fail');

	}

   
	//close statement 
	$stmt->close();

		} 

	} 

	
  //updating record
  if (isset($_POST['update'])) {
  	# code...
  	//capture the user input
  	# code...
	//capture the user input and validate the data 
	if (empty($_POST['moviename']) || empty($_POST['movielength']) || empty($_POST['moviegenre']) || empty($_POST['movieprice']) ) {
		# code...
		$errorMessage = "fields cannot be empty";
	} else {
		$updateid = $_POST['updateid'];
		$movie_name = $_POST['moviename'];
	    $movie_duration = $_POST['movielength'];
	    $movie_genre = $_POST['moviegenre'];
	    $movie_price = $_POST['movieprice'];
	    $movie_image = $_FILES['movieposter']['name'];
	}

	//target notation
	$target = "../../movieimages/" .basename($_FILES['movieposter']['name']);
	$storage_image = $_FILES['movieposter']['tmp_name'];
	//move the uploaded file to storage folder 
	if (move_uploaded_file($storage_image, $target) === TRUE) {
		# code...
		echo "<script>alert('image moved');</script>";
	} else {
				echo "<script>alert('image not moved');</script>";

	}
	// move_uploaded_file($storage_image, $target);

	if (empty($errorMessage)) {

 $updateSQL = "UPDATE movies SET moviename='$movie_name', movieduration='$movie_duration', moviegenre = '$movie_genre' , movieImage = '$movie_image', movieprice='$movie_price' WHERE id='$updateid' ";

		 $result = $conn->query($updateSQL);

		 if ($result === TRUE) {
		 	# code...
		 	$_SESSIONS['updateSuccess'] = "update Successful";
		    header('location: ../views/uploadMovieView.php?updateSuccess');
		 } else {
		 		# code...
		 	$_SESSIONS['fail'] = "update not Successful";
		    header('location: ../views/uploadMovieView.php?updatefail');
		    // echo $conn->error;
		 }

		} 

  }
	




?>