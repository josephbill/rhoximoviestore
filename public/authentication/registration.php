<?php
include '../../connection.php';

//variables to store input and also errors 
$moviestorename = $email = $password = $usertype = $enctyptedPass = '';
$inputerror = '';

//session variables 
$_SESSIONS['regsuccess'] = "Registration is successful";
$_SESSIONS['regnot'] = "Registration not successful";
$_SESSIONS['userTaken'] = "credentials already exists";

//capture user input 
if (isset($_POST['register'])) {
	# code...
	if (empty($_POST['email'] || empty($_POST['password']) )) {
		# code...
		$inputerror = "fields are empty";
	} else {
		$moviestorename = $_POST['moviestore'];
		$email = $_POST['email'];
		$password = $_POST['password'];
		$enctyptedPass = md5($password);
		$usertype = $_POST['usertype'];
	}

	//check if a user with the details already exists within our database table 
	$sql  = "SELECT * FROM users WHERE moviestorename='$moviestorename' && email='$email'";
	//execute query (mysqli procedural)
	$result = mysqli_query($conn,$sql);
	//check number of rows with similar records 
	$num = mysqli_num_rows($result);

	if ($num >= 1) {
		# code...
        header('location: ../../index.php?usertaken');
	} else {
		//proceed to create account 
		if (empty($inputerror)) {
			# code...
			$stmt = $conn->prepare( "INSERT INTO users (moviestorename,email,password,usertype) VALUES (?,?,?,?)");
			$stmt->bind_param('ssss',$moviestorename,$email,$enctyptedPass,$usertype);

			if ($stmt->execute() === TRUE) {
				# code...
				header('location: ../../index.php?userReg');
			} else {
			   header('location: ../../index.php?notReg');

			}
		} else {
			echo "error found, try again";
		}
	}

}





?>