<!DOCTYPE html>
<html>
<head>
	 <meta charset="UTF-8">
	  <meta name="description" content="movies">
	  <meta name="keywords" content="movies,genre,store">
	  <meta name="author" content="Joseph Mbugua">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rhoxi Movie Store</title>
	<!-- <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.min.css"> -->
	<link rel="stylesheet" type="text/css" href="../../css/style.css">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body>
	<br>
	<div class="container">
		<form action="../authentication/resetpass.php" method="post">
			<div class="form-group">
				<input type="email" name="emailForgot" class="form-control" placeholder="Enter account email">
			</div>
		    <div class="form-group">
					<input type="password" name="newpassword" class="form-control" placeholder="Enter new password">
			</div>
		    <div class="form-group">
					<input type="submit" name="resetpass" class="btn btn-success btn-sm" value="Forgot Password">
			</div>

		</form>
	</div>

</body>
</html>