<!DOCTYPE html>
<html>
<head>
	 <meta charset="UTF-8">
	  <meta name="description" content="movies">
	  <meta name="keywords" content="movies,genre,store">
	  <meta name="author" content="Joseph Mbugua">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rhoxi Movie Store</title>
	<!-- <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.min.css"> -->
	<link rel="stylesheet" type="text/css" href="../../css/style.css">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body style="background-image: url('images/body.jpg');">
    <?php
     require '../../connection.php';
    ?>
    <br>
	<div class="container">
       <div id="one">
		<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><img src="../../images/icon.png" style="width: 40px; height: 40px;"></a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
   <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="../../index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="viewMoviesView.php">View Movies</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="../authentication/logout.php">Logout</a>
      </li>
    </ul>
 
  </div>
</nav>
       </div>

       <div class="jumbotron" style="margin-top: 10px;">
            <div class="alert alert-<?php 
 
            if (isset($_GET['success'])) {
                   # code...
                     if (isset($_SESSIONS['classtype'])) {
                       # code... 
                        echo $_SESSIONS['classtype'];
                        session_unset();
                        session_destroy();
                     } else {
                        echo "success";
                     }
                 }
            ?>">
              <?php
                 if (isset($_GET['success'])) {
                   # code...
                     if (isset($_SESSIONS['success'])) {
                       # code... 
                        echo $_SESSIONS['success'];
                        session_unset();
                        session_destroy();
                     } else {
                        echo "movie uploaded";
                     }
                 }

                  if (isset($_GET['fail'])) {
                   # code...
                     if (isset($_SESSIONS['failure'])) {
                       # code... 
                        echo $_SESSIONS['failure'];
                        session_unset();
                        session_destroy();
                     } else {
                        echo "movie not uploaded";
                     }
                 }

                    if (isset($_GET['empty'])) {
                   # code...
                     if (isset($_SESSIONS['emptydata'])) {
                       # code... 
                        echo $_SESSIONS['emptydata'];
                        session_unset();
                        session_destroy();
                     } else {
                        echo "fields cannot be empty";
                     }
                 }


                    if (isset($_GET['updateSuccess'])) {
                   # code...
                     if (isset($_SESSIONS['updateSuccess'])) {
                       # code... 
                        echo $_SESSIONS['updateSuccess'];
                        session_unset();
                        session_destroy();
                     } else {
                        echo "update movie successful";
                     }
                 }
              ?>

            </div>
       	    <div class="container">
       	    	  <div class="row">
       	    	  	  <div class="col-sm" id="two">
       	    	  	  	  <h4>Upload Movie</h4>
       	    	  	  	  <hr>
       	    	  	  	  <form action="../administration/uploadMovie.php" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="hidden" name="updateid" placeholder="Enter Movie Name" class="form-control" value="<?php 
                                    
                                    echo $updateId;
 
                                ?>" required="">
                              </div>
       	    	  	  	  	    <div class="form-group">
       	    	  	  	  	    	<input type="text" name="moviename" placeholder="Enter Movie Name" class="form-control" value="<?php 
                                    
                                    echo $movie_name;
 
                                ?>" required="">
       	    	  	  	  	    </div>
       	    	  	  	  	       <div class="form-group">
       	    	  	  	  	    	<input type="number" name="movielength" placeholder="Enter Duration(hrs)" value="<?php 
                                    
                                    echo $movie_duration;
 
                                ?>" class="form-control" required="">
       	    	  	  	  	    </div>
       	    	  	  	  	       <div class="form-group">
       	    	  	  	  	    	<label>Pick Movie Genre</label>
       	    	  	  	  	    	<select class="form-control" name="moviegenre"> 
                                  <option value="<?php echo $movie_genre ?>"></option>
       	    	  	  	  	    		<option value="action">Action</option>
       	    	  	  	  	    		<option value="comedy">Comedy</option>
       	    	  	  	  	    		<option value="thriller">Thriller</option>
       	    	  	  	  	    	</select>
       	    	  	  	  	    </div>
       	    	  	  	  	       <div class="form-group">
       	    	  	  	  	    	<label>Upload Movie Poster</label>
       	    	  	  	  	    	<input type="file" name="movieposter" class="form-control">
       	    	  	  	  	    </div>
       	    	  	  	  	    <div class="form-group">
       	    	  	  	  	        <input type="text" name="movieprice" class="form-control" value="<?php 
                                    
                                    echo $movie_price;
 
                                ?>" placeholder="Enter Movie Price(KSH)">
       	    	  	  	  	    </div>
       	    	  	  	  	    <div class="form-group">
                                <?php
                                 if ($update == true) :
                                ?>
                                  <input type="submit" name="update" class="btn btn-warning btn-block" value="Update Movie" >
       	    	  	  	  	 
                               <?php
                                  else:
                                ?>
                                  <input type="submit" name="save" class="btn btn-success btn-block" value="Upload Movie" >
                               <?php
                                 endif;
                               ?>
       	    	  	  	  	    	<input type="reset" name="reset" class="btn btn-danger btn-block" value="Reset Form" >
       	    	  	  	  	    </div>
       	    	  	  	  </form>
       	    	  	  </div>
       	    	  	  
       	    	  </div>
       	    </div>
       </div>

       <div id="four">
       	   copyright Joseph @ <?php echo date('Y'); ?>
       </div>
		
	</div>



<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>