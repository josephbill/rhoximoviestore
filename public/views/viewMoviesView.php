<!DOCTYPE html>
<html>
<head>
	 <meta charset="UTF-8">
	  <meta name="description" content="movies">
	  <meta name="keywords" content="movies,genre,store">
	  <meta name="author" content="Joseph Mbugua">
	  <meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Rhoxi Movie Store</title>
	<!-- <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.css">
	<link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap-grid.min.css"> -->
	<link rel="stylesheet" type="text/css" href="../../css/style.css">

	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
</head>
<body style="background-image: url('images/body.jpg');">
    <br>
    <br>
    <!-- require connection -->
    <?php
        require '../../connection.php';
    ?>
	<div class="container">
       <div id="one">
		<nav class="navbar navbar-expand-lg navbar-light bg-primary">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="#"><img src="../../images/icon.png" style="width: 40px; height: 40px;"></a>

  <div class="collapse navbar-collapse" id="navbarTogglerDemo03">
    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
      <li class="nav-item active">
        <a class="nav-link" href="../../index.php">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="uploadMovieView.php">Upload Movies</a>
      </li>
      <li class="nav-item">
            <form action="#" method="post">
               <input type="text" name="searchValue" id="ss" placeholder="search movie" class="form-control">
               <br>
               <input type="submit" name="search" value="search" class="btn btn-success btn-sm">
            </form>       
       </li>
      <li class="nav-item">
        <a class="nav-link" href="#">Logout</a>
      </li>
    </ul>
 
  </div>
</nav>
       </div>

       <div class="jumbotron" style="margin-top: 10px;">
       	    <div class="container">
       	    	  <div class="row">
       	    	  	  <div class="col-sm" id="two">
                        <div class="alert alert-<?php 
                            if (isset($_GET['classtype'])) {
                               # code...
                                 if (isset($_SESSIONS['classtype'])) {
                                   # code...
                                   echo $_SESSIONS['classtype'];
                                 } else {
                                    echo "delete successful";
                                 }
                             }

                        ?>"> 
                            <?php
                             if (isset($_GET['classtype'])) {
                               # code...
                                 if (isset($_SESSIONS['deletesuccess'])) {
                                   # code...
                                   echo $_SESSIONS['deletesuccess'];
                                 } else {
                                    echo "delete successful";
                                 }
                             }
                            ?>
                        </div>
       	    	  	  	  <caption>Current Movies Uploaded</caption>
       	    	  	  	  <table class="table table-dark table-responsive">
       	    	  	  	  	  <tr>
       	    	  	  	  	  	<th>Movie Name</th>
       	    	  	  	  	  	<th>Movie Duration</th>
       	    	  	  	  	  	<th>Movie Genre</th>
       	    	  	  	  	  	<th>Movie Poster</th>
       	    	  	  	  	  	<th>Movie Price(KSH)</th>
       	    	  	  	  	  	<th colspan="2">Actions</th>
       	    	  	  	  	  </tr>

                            <!-- reading data  -->
                            <?php
                             $search_value = '';
                            //picking up my search tag
                            if (isset($_POST['search'])) {
                              # code...
                              //capturing user input 
                              if (empty($_POST['searchValue'])) {
                                # code...
                                echo "search cannot be empty. put a value";
                              } else {
                                $search_value = $_POST['searchValue'];
                              }
                            }

                            // $sql = "SELECT * FROM movies WHERE moviename = '$search_value'";
                            $sql = "SELECT * FROM movies";
                            $result = $conn->query($sql);
                            // var_dump($result);
                            //looping the records
                            while ($row = $result->fetch_assoc()) :
                            ?>

       	    	  	  	  	  <tr>
       	    	  	  	  	  	<td><?php echo $row['moviename']; ?></td>
       	    	  	  	  	  	<td><?php echo $row['movieduration']; ?></td>
       	    	  	  	  	  	<td><?php echo $row['moviegenre']; ?></td>
       	    	  	  	  	  	<td>
       	    	  	  	  	  	 <?php 
                                echo "<a href='
                                     ../../movieimages/" . $row['movieImage'] . "
                                ' target='_blank'><img src='
                                   ../../movieimages/" . $row['movieImage'] . "
                                ' style='width: 120px; height: 120px;'/></a>";

                               ?>
       	    	  	  	  	  	
       	    	  	  	  	  	</td>
       	    	  	  	  	  	<td><?php echo $row['movieprice']; ?></td>
       	    	  	  	  	  	<td>
       	    	  	  	  	  		<a href="uploadMovieView.php?edit=<?php echo $row['id'] ?>" type="btn" class="btn btn-primary">Edit</a>

       	    	  	  	  	  		<a href="../../connection.php?delete=<?php

                                     echo $row['id'];
                                ?> 
                                " type="btn" class="btn btn-danger">Delete</a>
       	    	  	  	  	  	</td>
       	    	  	  	  	  </tr>
                            <?php
                            endwhile;
                            ?>
       	    	  	  	  </table>
       	    </div>
       </div>

       <div id="four">
       	   copyright Joseph @ <?php echo date('Y'); ?>
       </div>
		
	</div>
</div>



<script type="text/javascript" src="js/script.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.js"></script>
<script type="text/javascript" src="bootstrap/js/bootstrap.min.js"></script>
</body>
</html>