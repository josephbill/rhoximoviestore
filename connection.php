<?php
session_start();
//connecting to mysql database 
$servername = "localhost";
$username = "root";
$password = "";
$dbname = "rhoximoviestore";


//using mysqli_oop to create connection
//we need an instance of the mysqli class 
$conn = new mysqli($servername,$username,$password,$dbname);

//checking if connection is established 
if ($conn->connect_error) {
	# code...
		echo "connection not successful";

} 

//variable for delete
$id = 0;
$updateId = 0;
$update = false;
//variable to store existing data fetched from database for update process
$movie_name = '';
$movie_duration = '';
$movie_genre = '';
$movie_poster = '';
$movie_price = '';

//  else {
// 	echo "connection established";
// }

//creating database using SQL 
// $sql = "CREATE DATABASE rhoximoviestore";
// //now execute sql 
// if ($conn->query($sql) === TRUE) {
// 	# code...
// 	echo "database has been created";
// } else {
// 	echo "database not created" . $conn->error;
// }

// //creating tables within my database
// $sql = "CREATE TABLE movies(
//  id INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
//  moviename VARCHAR(250) NOT NULL,
//  movieduration VARCHAR(250) NOT NULL,
//  moviegenre VARCHAR(250) NOT NULL,
//  movieImage VARCHAR(250) NOT NULL,
//  movieprice INT(11) NOT NULL,
// creationdate TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP

// )";

// if ($conn->query($sql) === TRUE) {
// 	# code...
// 	echo "table created";
// } else {
// 	echo "table not created" . $conn->error;
// }

//delete logic
if(isset($_GET['delete'])){
	//pick the id 
    $id  = $_GET['delete'];

    $deletesql = "DELETE FROM movies WHERE id='$id'";
    if ($conn->query($deletesql) === TRUE) {
     	# code...
     	//redirects and message
     	$_SESSIONS['deletesuccess'] = "Movie deleted";
     	$_SESSIONS['classtype'] = "danger";
     	header('location: public/views/viewMoviesView.php?classtype');

     }  else {
     		//redirects and message
     	$_SESSIONS['deletefail'] = "Movie deleted";
     	header('location: public/views/viewMoviesView.php?dfail');
     }
}

//update logic
if (isset($_GET['edit'])) {
	# code...
	$id = $_GET['edit'];
    $update = true;
	//pick the current values associated with this record 
	$sql = "SELECT * FROM movies WHERE id='$id'";
    $result = $conn->query($sql);
    $row = $result->fetch_array();

    $updateId = $row['id'];
    $movie_name = $row['moviename'];
    $movie_duration = $row['movieduration'];
    $movie_genre = $row['moviegenre'];
    $movie_price = $row['movieprice'];


}





?>